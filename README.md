# Rainfuck
A configurable, minimal, powerful Brainfuck interpreter library!
Rainfuck uses no dependencies unless you specify a feature that requires one!



# Examples:
```rust
use rainfuck::BrainfuckVM;

// A simple cat program. (Just repeats what you type.)
fn main() {
    let mut vm = BrainfuckVM::new();
    vm.prompt_display = String::from("[TYPE]: ");

    vm.process(",[.,]");
}
```
```rust
use rainfuck::BrainfuckVM;

// Run some stuff, then reset the virtual machine.
fn main() {
    let mut vm = BrainfuckVM::new();
    assert!(vm == BrainfuckVM::new());

    vm.process("++++++++[>+>+<<-]>>[<<+>>-]");
    assert!(vm.cells[0..4] == [8, 8, 0, 0]);

    vm.reset();
    assert!(vm == BrainfuckVM::new());
}
```
