//! # Rainfuck
//! A configurable, minimal, powerful Brainfuck interpreter library!
//! Rainfuck uses no dependencies unless you specify a feature that requires one!
//!
//!
//!
//! # Examples:
//! ```rust
//! use rainfuck::BrainfuckVM;
//!
//! // A simple cat program. (Just repeats what you type.)
//! fn main() {
//!     let mut vm = BrainfuckVM::new();
//!     vm.prompt_display = String::from("[TYPE]: ");
//!
//!     vm.process(",[.,]");
//! }
//! ```
//! ```rust
//! use rainfuck::BrainfuckVM;
//!
//! // Run some stuff, then reset the virtual machine.
//! fn main() {
//!     let mut vm = BrainfuckVM::new();
//!     assert!(vm == BrainfuckVM::new());
//!
//!     vm.process("++++++++[>+>+<<-]>>[<<+>>-]");
//!     assert!(vm.cells[0..4] == [8, 8, 0, 0]);
//!
//!     vm.reset();
//!     assert!(vm == BrainfuckVM::new());
//! }
//! ```

use std::io::Write;

#[cfg(feature = "debug-colors")]
use owo_colors::OwoColorize;

#[cfg(feature = "serde")]
#[derive(serde::Serialize, serde::Deserialize)]
/// Brainfuck virtual machine (that can be serialized and deserialized)
/// This cannot be done with the normal BrainfuckVM, because it uses a stack array with 30,000 elements.
pub struct BrainfuckVMSerde {
    pub debug: bool,
    pub prompt_display: String,
    pub cells: Vec<u8>,
    pub pointer: u16,
    pub scope: usize,
    pub input_buffer: Vec<char>,
    pub output: String,
}

#[cfg(feature = "serde")]
impl BrainfuckVMSerde {
    /// BrainfuckVM -> BrainfuckVMSerde
    pub fn from(vm: BrainfuckVM) -> Self {
        let mut cells_vec = vm.cells.to_vec();

        if let Some(s) = cells_vec.iter().rposition(|x| *x != 0) {
            cells_vec.truncate(s + 1);
        }

        return Self {
            debug: vm.debug,
            prompt_display: vm.prompt_display,
            cells: cells_vec,
            pointer: vm.pointer,
            scope: vm.scope,
            input_buffer: vm.input_buffer,
            output: vm.output,
        };
    }

    /// BrainfuckVMSerde -> BrainfuckVM
    pub fn to_normal(self) -> BrainfuckVM {
        let mut cells: [u8; 30_000] = [0; 30_000];

        for i in 0..self.cells.len() {
            cells[i] = self.cells[i];
        }

        return BrainfuckVM {
            debug: self.debug,
            prompt_display: self.prompt_display,
            cells,
            pointer: self.pointer,
            scope: self.scope,
            input_buffer: self.input_buffer,
            output: self.output,
        };
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
/// Brainfuck virtual machine
///
/// '(Read)' ~ Please only read it. (Unless you need to write to it for some very weird reason!)
///
/// '(Read & Write)' ~ Read it, or write to it!
pub struct BrainfuckVM {
    /// (Read & Write) Enable/disable debug messages (use the debug-colors feature for pretty debug messages)
    pub debug: bool,
    /// (Read & Write) A custom prefix for the user input operation (',')
    pub prompt_display: String,
    /// (Read) The memory cells of the virtual machine
    pub cells: [u8; 30_000],
    /// (Read) The pointer to the current memory cell location
    pub pointer: u16,
    /// (Read) The current scope (Outside of all loops is scope 0)
    pub scope: usize,
    /// (Read) The user input buffer
    pub input_buffer: Vec<char>,
    /// (Read) The program output (What was printed)
    pub output: String,
}

impl BrainfuckVM {
    /// Create new Brainfuck virtual machine
    ///
    /// # Examples:
    /// ```rust
    /// use rainfuck::BrainfuckVM;
    ///
    /// fn main() {
    ///     // Create a virtual machine
    ///     let mut vm = BrainfuckVM::new();
    ///
    ///     // (Optional): Configure the virtual machine
    ///     vm.debug = true; // Debug messages (what the interpreter is doing)
    ///     vm.prompt_display = String::from("-> "); // Custom prefix for the ',' (user input) command
    /// }
    /// ```
    pub fn new() -> Self {
        return Self {
            debug: false,
            prompt_display: String::new(),
            cells: [0; 30_000],
            pointer: 0,
            scope: 0,
            input_buffer: Vec::new(),
            output: String::new(),
        };
    }

    /// Reset virtual machine
    ///
    /// # Examples:
    /// ```rust
    /// use rainfuck::BrainfuckVM;
    ///
    /// fn main() {
    ///     let mut vm = BrainfuckVM::new();
    ///
    ///     vm.process("++++++++[>+<-]");
    ///     assert!(vm != BrainfuckVM::new());
    ///
    ///     vm.reset();
    ///     assert!(vm == BrainfuckVM::new());
    /// }
    /// ```
    pub fn reset(&mut self) {
        *self = Self::new();
    }

    /// Process some Brainfuck code
    /// The virtual machine actually preserves the state
    /// between each process() call, so you can chain them together!
    ///
    /// # Examples:
    /// ```rust
    /// use rainfuck::BrainfuckVM;
    ///
    /// fn main() {
    ///     let mut vm = BrainfuckVM::new();
    ///
    ///     // Print: 'A' (ASCII: 65)
    ///     vm.process("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++.");
    ///
    ///     // Print: 'B' (ASCII: 66)
    ///     vm.process("+.");
    ///
    ///     // Print: 'C' (ASCII: 67)
    ///     vm.process("+.");
    ///
    ///     assert!(vm.cells[0] == 67);
    ///     assert!(vm.output.trim() == "ABC");
    /// }
    /// ```
    pub fn process(&mut self, code: &str) {
        let mut position: usize = 0;

        let code: Vec<char> = code.chars().collect();

        while position < code.len() {
            let c = code[position];

            #[cfg(not(feature = "debug-colors"))]
            match self.debug { true => print!("[scope: {}, pointer: {}] eval: '{c}' -> ", self.scope, self.pointer), false => () };

            #[cfg(feature = "debug-colors")]
            match self.debug { true => print!(
                "[scope: {}, pointer: {}] eval: '{c}' -> ",
                self.scope.bright_cyan(),
                self.pointer.bright_magenta(),
                c = c.bright_yellow().bold()),
                false => (),
            };

            match c {
                '+' => {
                    self.cells[self.pointer as usize] = self.cells[self.pointer as usize].overflowing_add(1).0;

                    #[cfg(not(feature = "debug-colors"))]
                    match self.debug { true => print!("add one"), false => () };

                    #[cfg(feature = "debug-colors")]
                    match self.debug { true => print!("{}", "add one".bright_green()), false => () };
                },
                '-' => {
                    self.cells[self.pointer as usize] = self.cells[self.pointer as usize].overflowing_sub(1).0;

                    #[cfg(not(feature = "debug-colors"))]
                    match self.debug { true => print!("subtract one"), false => () };

                    #[cfg(feature = "debug-colors")]
                    match self.debug { true => print!("{}", "subtract one".bright_red()), false => () };
                },
                '>' => {
                    if self.pointer as usize == self.cells.len() - 1 {
                        self.pointer = 0;

                        #[cfg(not(feature = "debug-colors"))]
                        match self.debug { true => print!("move right one (overflow)"), false => () };

                        #[cfg(feature = "debug-colors")]
                        match self.debug { true => print!("{}", "move right one (overflow)".bright_green()), false => () };
                    } else {
                        self.pointer += 1;

                        #[cfg(not(feature = "debug-colors"))]
                        match self.debug { true => print!("move right one"), false => () };

                        #[cfg(feature = "debug-colors")]
                        match self.debug { true => print!("{}", "move right one".bright_green()), false => () };
                    }
                },
                '<' => {
                    if self.pointer as usize == 0 {
                        self.pointer = self.cells.len() as u16 - 1;

                        #[cfg(not(feature = "debug-colors"))]
                        match self.debug { true => print!("move left one (overflow)"), false => () };

                        #[cfg(feature = "debug-colors")]
                        match self.debug { true => print!("{}", "move left one (overflow)".bright_red()), false => () };
                    } else {
                        self.pointer -= 1;

                        #[cfg(not(feature = "debug-colors"))]
                        match self.debug { true => print!("move left one"), false => () };

                        #[cfg(feature = "debug-colors")]
                        match self.debug { true => print!("{}", "move left one".bright_red()), false => () };
                    }
                },
                '.' => {
                    let character: char = String::from_utf8_lossy(&vec![self.cells[self.pointer as usize]]).chars().into_iter().collect::<Vec<char>>()[0];

                    self.output.push(character);

                    match self.debug { true => println!(""), false => () };

                    print!("{character}");
                },
                ',' => {
                    if self.input_buffer.len() == 0 {
                        let mut user_input = String::new();
                        print!("{}", self.prompt_display);
                        std::io::stdout().flush().unwrap();
                        std::io::stdin().read_line(&mut user_input).unwrap();

                        for i in user_input.chars() {
                            self.input_buffer.push(i);
                        }
                    }

                    if self.input_buffer.len() > 0 {
                        self.cells[self.pointer as usize] = self.input_buffer.remove(0) as u8;
                    }

                    else {
                        self.cells[self.pointer as usize] = 0;
                    }
                },
                '[' => {
                    let mut inner_code: Vec<char> = Vec::new();

                    let mut offset: usize = 1;
                    let mut hits: usize = 1;
                    let start: usize = position + offset;
                    let end: usize;
                    loop {
                        let character = match code.get(position + offset) {
                            Some(s) => *s,
                            None => panic!("Invalid loop! Missing ']' bracket to close it!"),
                        };

                        if character == '[' { hits += 1 }
                        if character == ']' { hits -= 1 }

                        if hits == 0 {
                            end = (position + offset) - 1;

                            break;
                        }

                        inner_code.push(character);

                        offset += 1;
                    }

                    let inner_code: String = inner_code.into_iter().collect();

                    self.scope += 1;
                    while self.cells[self.pointer as usize] != 0 {
                        #[cfg(not(feature = "debug-colors"))]
                        match self.debug { true => println!("loop eval '{}' ( start: {start}, end: {end} )", inner_code), false => () };

                        #[cfg(feature = "debug-colors")]
                        match self.debug { true => println!(
                            "loop eval '{}' ( start: {start}, end: {end} )",
                            inner_code.bright_cyan(),
                            start = start.bright_blue(),
                            end = end.bright_green()),
                            false => ()
                        };

                        self.process(&inner_code);
                    }
                    self.scope -= 1;

                    position = end + 1;
                },
                _ => (),
            };

            match self.debug { true => println!(""), false => () };

            position += 1;
        }
    }
}

#[test]
fn hello_world_test() {
    let mut vm = BrainfuckVM::new();

    vm.process("[-][Hello, world!]++++++++[>++++>++++++>++++++++>++++++++++>++++++++++++<<<<<-]>>>++++++++.>>+++++.+++++++..+++.<<<----.<.>>>>++++++++.--------.+++.------.--------.<<<<+.[>]<[[-]<]");

    assert!(vm.output.trim() == "Hello, world!");
}

#[test]
fn reset_test() {
    let mut vm = BrainfuckVM::new();

    assert!(vm.cells[0] == 0);
    assert!(vm.cells[1] == 0);

    vm.process("++++++++[>+<-]");

    assert!(vm.cells[0] == 0);
    assert!(vm.cells[1] == 8);

    vm.reset();

    assert!(vm.cells[0] == 0);
    assert!(vm.cells[1] == 0);
}
